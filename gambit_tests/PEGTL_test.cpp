//
// Created by sudeep on 05/02/17.
//

// includes pegtl.h
#include "Libs.h"

// includes gtest
#include "Test_Libs.h"

namespace hello {
// Parsing rule for matching a literal "Hello"
struct prefix : pegtl::string<'H', 'e', 'l', 'l', 'o', ',', ' '> {};

// Parsing rule matching non-empty sequencey of alpha-numeric
// ascii-characters with greedy-matching
struct name : pegtl::plus<pegtl::alpha> {};

// Parsing rule that matches prefix, name, '!' and eof
struct grammar : pegtl::must<prefix, name, pegtl::one<'!'>, pegtl::eolf> {};

// Class template for user-defined actions that does nothing
template <typename Rule> struct action : pegtl::nothing<Rule> {};

// Specialization of user-defined action to do something when
// 'name' rule succeeds; is called with portion of the input that
// matched with the rule
template <> struct action<name> {
  template <typename Input>
  static void apply(const Input &in, std::string &name) {
    name = in.string();
  }
};
} // namespace hello

TEST(PEGTL_test, hello_world_test) {
  std::string h1("Hello, World!");
  std::string h2("Hello, Dee!");

  std::string name;

  try {
    pegtl::parse_string<hello::grammar, hello::action>(h2, std::string(""),
                                                       name);
  } catch (const std::exception &e) {
    std::cerr << "Parse Error: " << e.what() << std::endl;
    FAIL();
  }
  std::cout << "Name: " << name << std::endl;
}

namespace colon_separated {
using namespace pegtl;
// Parsing rule for an integer
//struct integer : plus<digit> {};

// Parsing rule for register
struct reg_value : plus<digit> {};

// Parsing rule for value
struct val_value : plus<digit> {};

// Parsing rule to parse colon separated integers
struct reg_value_pair : must<reg_value, pegtl::one<':'>, val_value> {};

// entire grammar
struct grammar : seq<list_tail<reg_value_pair, space>, eolf> {};

// Class template for user defined action that does nothing
template <typename Rule> struct extract_pairs : nothing<Rule> {};

// Action to extract register
template <> struct extract_pairs<reg_value> {
  template <typename Input>
  static void apply(const Input &in, std::vector<int> &reg, std::vector<int> & /*val*/) {
    std::string reg_str = in.string();
    reg.emplace_back(std::stoi(reg_str));
  }
};

// Action to extract value
template <> struct extract_pairs<val_value> {
  template <typename Input>
  static void apply(const Input &in, std::vector<int> & /*reg*/, std::vector<int> &val) {
    std::string val_str = in.string();
    val.emplace_back(std::stoi(val_str));
  }
};

// Action to print both value
template<> struct extract_pairs<reg_value_pair> {
  template<typename Input>
  static void apply(const Input &in, std::vector<int> & /*reg*/, std::vector<int> & /*val*/) {
    std::string reg_value_pair{in.string()};
    std::cout << reg_value_pair << std::endl;
  }
};
} // namespace colon_separated

TEST(PEGTL__Test, colon_separated_test) {
  std::string c1("123:243 321:234");
  std::string c2("123:243 321:234 ");

  std::vector<int> regs;
  std::vector<int> vals;

  std::vector<int> ref_regs = {123, 321};
  std::vector<int> reg_vals = {243, 234};

  try {
    pegtl::parse_string<colon_separated::grammar, colon_separated::extract_pairs>(
        c1, std::string(""), regs, vals);
  } catch (const std::exception &e) {
    std::cerr << "Parse Error: " << e.what() << std::endl;
    FAIL();
  }

  for (int i = 0; i < static_cast<int>(regs.size()); ++i) {
    std::cout << "Register[" << i << "] = " << regs[static_cast<size_t>(i)] << std::endl;
    ASSERT_EQ(regs[static_cast<size_t>(i)], ref_regs[static_cast<size_t>(i)]);
    std::cout << "Value[" << i << "] = " << vals[static_cast<size_t>(i)] << std::endl;
    ASSERT_EQ(vals[static_cast<size_t>(i)], reg_vals[static_cast<size_t>(i)]);
  }
}

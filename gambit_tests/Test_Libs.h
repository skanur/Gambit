//
// Created by sudeep on 09/02/17.
//

#ifndef GAMBIT_TEST_LIBS_H
#define GAMBIT_TEST_LIBS_H

// Avoid warnings for these libraries
#ifdef __GNUC__
#pragma GCC system_header
#endif

#ifdef __clang__
#pragma clang system_header
#endif

// TODO: Add similar ignores for MSVC
#include "gtest/gtest.h"

#endif //GAMBIT_TEST_LIBS_H

//
// Created by sudeep on 04/02/17.
//

#include <Libs.h>
#include <Types.h>
#include <fstream>

// includes gtest.h
#include "Test_Libs.h"

using nlohmann::json;

class JsonModBusConfigFixture : public ::testing::Test {
protected:
  json config_json_obj;
  std::string json_filename;
  void SetUp() override;
  ~JsonModBusConfigFixture() override = default;
};

void JsonModBusConfigFixture::SetUp() {
  json_filename = std::string("../../data/modbus_config.json");
  std::ifstream json_file_stream;

  json_file_stream.open(json_filename);
  if (json_file_stream.bad() || json_file_stream.fail()) {
    std::cerr << "Could not open: " << json_filename << std::endl;
  } else if (json_file_stream.good()) {
    // Read in the json config file to the object
    try {
      json_file_stream >> config_json_obj;
    } catch (const std::exception &e) {
      json_file_stream.close();
      std::cerr << e.what() << std::endl;
    }
    json_file_stream.close();
  }
}

TEST_F(JsonModBusConfigFixture, to_vector) {
  for (uint i = 0; i < config_json_obj["modbus_config"].size(); i++) {
    json modbusrow_json = config_json_obj["modbus_config"][i];
    std::cout << modbusrow_json["variable_name"].get<std::string>()
              << std::endl;
  }
}


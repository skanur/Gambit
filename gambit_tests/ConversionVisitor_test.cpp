//
// Created by sudeep on 02/02/17.
//

// Includes gtest.h
#include "Test_Libs.h"

#include "ConversionException.h"
#include "ConvertVisitor.h"
#include "Real.h"
#include "Types.h"

#include <string>
#include <utility>
#include <vector>

const std::string EMPTYSTRING;

struct ConvertTypesFixture : public ::testing::Test {
  ConvertVisitor cv{};
};

TEST_F(ConvertTypesFixture, temperature) {
  Real r("Temperature", "C", 15568, 16611);
  r.accept(cv);
  EXPECT_FLOAT_EQ(7.101173400878906, r._real);
}

TEST_F(ConvertTypesFixture, energy_accumulator) {
  Long l(EMPTYSTRING, EMPTYSTRING, static_cast<const t_value>(65480), static_cast<const t_value>(65535));
  l.accept(cv);
  EXPECT_EQ(-56, l._long);
}

TEST_F(ConvertTypesFixture, system_password) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(49, 0x0000));
  bcd_reg_map.emplace_back(std::make_pair(50, 0x00A0));

  BCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  b.accept(cv);
  EXPECT_EQ("00A00000", b._bcd);
}

TEST_F(ConvertTypesFixture, calendar) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2543)); // 25 mins, 43 s

  bcd_reg_map.emplace_back(std::make_pair(54, 0x0212)); // 2nd _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1801)); // 18th _year, 1st _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  b.accept(cv);
  EXPECT_EQ("180102122543", b._bcd);
}

TEST_F(ConvertTypesFixture, calendar_wrong_seconds) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2561)); // 25 mins, 61 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x0212)); // 2nd _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1801)); // 18th _year, 1st _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  try {
    b.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ(
        "ConversionException: Calendar's Second/Minute: 61 is not within 60",
        err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, calendar_wrong_mintues) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x6143)); // 61 mins, 43 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x0212)); // 2nd _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1801)); // 18th _year, 1st _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  try {
    b.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ(
        "ConversionException: Calendar's Second/Minute: 61 is not within 60",
        err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, calendar_wrong_hour) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2543)); // 25 mins, 43 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x0224)); // 2nd _day, 24 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1801)); // 18th _year, 1st _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  try {
    b.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ("ConversionException: Calendar's Hour: 24 is not within 24",
                 err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, calendar_wrong_day) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2543)); // 25 mins, 43 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x3212)); // 32st _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1801)); // 18th _year, 1st _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  try {
    b.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ("ConversionException: Calendar's Date: 32 is not within 31",
                 err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, calendar_wrong_month) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2543)); // 25 mins, 43 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x0212)); // 2nd _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1813)); // 18th _year, 13th _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  try {
    b.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ("ConversionException: Calendar's Month: 13 is not within 12",
                 err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, calendar_non_leap_w_days_feb) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2543)); // 25 mins, 43 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x2912)); // 29th _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1802)); // 18th _year, 02nd _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  try {
    b.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ(
        "ConversionException: Non-Leap _year February can't have 28 days",
        err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, calendar_leap_w_days_apr) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2543)); // 25 mins, 43 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x3112)); // 31st _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1604)); // 18th _year, 04th _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  try {
    b.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ("ConversionException: Month: 4 can't have more than 30 days",
                 err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, calendar_leap_c_days_feb) {
  std::vector<std::pair<t_reg, t_value>> bcd_reg_map;
  bcd_reg_map.emplace_back(std::make_pair(53, 0x2543)); // 25 mins, 43 s
  bcd_reg_map.emplace_back(std::make_pair(54, 0x2912)); // 29th _day, 12 hours
  bcd_reg_map.emplace_back(std::make_pair(55, 0x1602)); // 18th _year, 02nd _month
  CalendarBCD b(EMPTYSTRING, EMPTYSTRING, bcd_reg_map);
  b.accept(cv);
  EXPECT_EQ("160229122543", b._bcd);
}

TEST_F(ConvertTypesFixture, partition_high_mask) {
  PartitionedInteger p(EMPTYSTRING, EMPTYSTRING, 0x2320, 100, HIGH_HW);
  p.accept(cv);
  EXPECT_EQ(0x0023, p._integer);
}

TEST_F(ConvertTypesFixture, partition_low_mask) {
  PartitionedInteger p(EMPTYSTRING, EMPTYSTRING, 0x2320, 100, LOW_HW);
  p.accept(cv);
  EXPECT_EQ(0x0020, p._integer);
}

TEST_F(ConvertTypesFixture, partition_beyond_value) {
  PartitionedInteger p(EMPTYSTRING, EMPTYSTRING, 0x2320, 0x20, HIGH_HW);
  try {
    p.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ(
        "ConversionException: Integer: 35 can't be above max bound: 32",
        err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

TEST_F(ConvertTypesFixture, signal_quality) {
  PartitionedInteger p(EMPTYSTRING, EMPTYSTRING, 806, 99, LOW_HW);
  p.accept(cv);
  EXPECT_EQ(38, p._integer);
}

TEST_F(ConvertTypesFixture, working_step) {
  PartitionedInteger p(EMPTYSTRING, EMPTYSTRING, 0x2032, 99, HIGH_HW);
  p.accept(cv);
  EXPECT_EQ(0x0020, p._integer);
}

TEST_F(ConvertTypesFixture, language_english) {
  LanguageInteger l(EMPTYSTRING, EMPTYSTRING, 0);
  l.accept(cv);
  EXPECT_EQ(std::string("English"), l._lang);
}

TEST_F(ConvertTypesFixture, language_chinese) {
  LanguageInteger l(EMPTYSTRING, EMPTYSTRING, 1);
  l.accept(cv);
  EXPECT_EQ(std::string("Chinese"), l._lang);
}

TEST_F(ConvertTypesFixture, language_not_supported) {
  LanguageInteger l(EMPTYSTRING, EMPTYSTRING, 2);
  try {
    l.accept(cv);
    FAIL() << "Expected: ConversionException";
  } catch (const ConversionException &err) {
    ASSERT_STREQ("ConversionException: Option: 2 not supported", err.what());
  } catch (...) {
    FAIL() << "Expected: ConversionException";
  }
}

//
// Created by sudeep on 09/02/17.
//

#include "Libs.h"
#include "ModbusParser.h"
#include "AbstractParsedAST.h"
#include "ParsedAST.h"

#include "Test_Libs.h"

using namespace modbusparser;

class ModbusParserFixture : public ::testing::Test {
protected:
  std::shared_ptr<AbstractParsedAST> p;

  ModbusParserFixture();
  ~ModbusParserFixture() override;
};

ModbusParserFixture::ModbusParserFixture() {
  p = std::shared_ptr<AbstractParsedAST>(new ParsedAST());
}

ModbusParserFixture::~ModbusParserFixture() = default;

TEST_F(ModbusParserFixture, hour_test) {
  std::string s("12:12");

  try {
    parse_string<modbusparser::time, modbusparser::action>(s, std::string(""), p);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }
  ASSERT_EQ(p->get_time(), s);
}

TEST_F(ModbusParserFixture, date_test) {
  std::string s("2017-92-32");

  try {
    parse_string<modbusparser::date, modbusparser::action>(s, std::string(""), p);
  } catch(const std::exception& e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  ASSERT_EQ(p->get_date(), s);
}

TEST_F(ModbusParserFixture, timestamp_test) {
  std::string s("2912-23-43 23:59");

  try {
    parse_string<modbusparser::timestamp, modbusparser::action>(s, std::string(""), p);
  } catch(const std::exception& e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  ASSERT_EQ(p->get_timestamp(), s);
}

TEST_F(ModbusParserFixture, timestamp_all_zeros) {
  std::string s("0000-00-00 00:00");

  try {
    parse_string<modbusparser::timestamp, modbusparser::action>(s, std::string(""), p);
  } catch(const std::exception& e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  ASSERT_EQ(p->get_timestamp(), s);
}

TEST_F(ModbusParserFixture, reg_value_test) {
  std::string s("12:65535\n");

  try{
    parse_string<modbusparser::reg_value, modbusparser::action>(s, std::string(""), p);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  auto reg_vals{p->get_regVals()};
  ASSERT_EQ(reg_vals.size(), 1);
  auto first_el = reg_vals.begin();
  ASSERT_EQ(first_el->first, 12);
  ASSERT_EQ(first_el->second, 65535);
}

TEST_F(ModbusParserFixture, list_reg_value_test) {
  std::string s("12:2312\r\n23:23214\n243:2345\n");

  try{
    parse_string<modbusparser::regs_values, modbusparser::action>(s, std::string(""), p);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  auto reg_vals{p->get_regVals()};
  ASSERT_EQ(reg_vals.size(), 3);

  auto reg_val_it = reg_vals.begin();
  ASSERT_EQ(reg_val_it->first, 12);
  ASSERT_EQ(reg_val_it->second, 2312);

  reg_val_it++;
  ASSERT_EQ(reg_val_it->first, 23);
  ASSERT_EQ(reg_val_it->second, 23214);

  reg_val_it++;
  ASSERT_EQ(reg_val_it->first, 243);
  ASSERT_EQ(reg_val_it->second, 2345);
}

TEST_F(ModbusParserFixture, grammar_linux_newline_end_test) {
  std::string s("2343-23-24 23:34\n12:2312\r\n23:23214\n243:2345\n");

  try{
    parse_string<modbusparser::grammar, modbusparser::action>(s, std::string(""), p);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  ASSERT_EQ(p->get_timestamp(), std::string("2343-23-24 23:34"));

  auto reg_vals{p->get_regVals()};
  ASSERT_EQ(reg_vals.size(), 3);

  auto reg_val_it = reg_vals.begin();
  ASSERT_EQ(reg_val_it->first, 12);
  ASSERT_EQ(reg_val_it->second, 2312);

  reg_val_it++;
  ASSERT_EQ(reg_val_it->first, 23);
  ASSERT_EQ(reg_val_it->second, 23214);

  reg_val_it++;
  ASSERT_EQ(reg_val_it->first, 243);
  ASSERT_EQ(reg_val_it->second, 2345);
}

TEST_F(ModbusParserFixture, grammar_newline_end_test) {
  std::string s("2343-23-24 23:34\n12:2312\r\n23:23214\n243:2345\r\n");

  try{
    parse_string<modbusparser::grammar, modbusparser::action>(s, std::string(""), p);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  ASSERT_EQ(p->get_timestamp(), std::string("2343-23-24 23:34"));

  auto reg_vals{p->get_regVals()};
  ASSERT_EQ(reg_vals.size(), 3);

  auto reg_val_it = reg_vals.begin();
  ASSERT_EQ(reg_val_it->first, 12);
  ASSERT_EQ(reg_val_it->second, 2312);

  reg_val_it++;
  ASSERT_EQ(reg_val_it->first, 23);
  ASSERT_EQ(reg_val_it->second, 23214);

  reg_val_it++;
  ASSERT_EQ(reg_val_it->first, 243);
  ASSERT_EQ(reg_val_it->second, 2345);
}

TEST_F(ModbusParserFixture, file_read_test) {
  try{
    parse_file<modbusparser::grammar, modbusparser::action>("../../data/sample_input1.dat", p);
  } catch(const std::exception& e){
    std::cerr << e.what() << std::endl;
    FAIL();
  }

  ASSERT_EQ(p->get_timestamp(), std::string("2017-01-11 19:12"));

  auto reg_vals{p->get_regVals()};
  ASSERT_EQ(reg_vals.size(), 100);

  auto reg_val_fw_it = reg_vals.begin();
  ASSERT_EQ(reg_val_fw_it->first, 1);
  ASSERT_EQ(reg_val_fw_it->second, 7579);

  reg_val_fw_it++;
  ASSERT_EQ(reg_val_fw_it->first, 2);
  ASSERT_EQ(reg_val_fw_it->second, 48988);

  auto reg_val_bw_it = reg_vals.rbegin();
  ASSERT_EQ(reg_val_bw_it->first, 100);
  ASSERT_EQ(reg_val_bw_it->second, 17839);
}

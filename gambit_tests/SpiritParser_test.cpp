//
// Created by sudeep on 04/02/17.
//

//#include <boost/spirit/home/x3.hpp>
//#include <boost/fusion/include/std_pair.hpp>
//#include <boost/config/warning_disable.hpp>
//
//// include gtest.h
//#include "Test_Libs.h"
//
//#include <complex>
//#include <string>
//
// using namespace boost::spirit;
//
// template <typename Iterator>
// bool parse_numbers_vector(Iterator first, Iterator last,
// std::vector<double>&v) {
//    auto push = [&](auto& ctxt) { v.emplace_back(x3::_attr(ctxt));};
//    bool r = x3::phrase_parse(
//            first,
//            last,
//            // Begin grammar
//            (
//                    x3::double_[push] >> *("," >> x3::double_[push])
//            ),
//            // End grammar
//            x3::ascii::space
//    );
//
//    if(!r || first!=last)
//        return false;
//    return r;
//}
//
// TEST(Spirit_test, parse_numbers_vector_test) {
//    std::string in1("1.0, 1, 1.0, 1");
//    std::vector<double> result;
//    parse_numbers_vector(in1.begin(), in1.end(), result);
//    EXPECT_EQ(result.size(), (unsigned int) 4);
//    for(auto&& num: result) {
//        EXPECT_DOUBLE_EQ(1.0, num);
//    }
//}
//
// template <typename Iterator>
// bool parse_numbers_sum(Iterator first, Iterator last, double& n) {
//    auto assign = [&](auto& ctxt) {n = x3::_attr(ctxt);};
//    auto sum = [&](auto& ctxt) {n += x3::_attr(ctxt);};
//    bool r = x3::phrase_parse (
//            first,
//            last,
//            // Begin grammar
//            (
//                    x3::double_[assign] >> *("," >> x3::double_[sum])
//            ),
//            // End grammer
//            x3::ascii::space
//    );
//
//    if(!r || first != last)
//        return false;
//    return r;
//}
//
// TEST(Spirit_test, parse_numbers_sum_test) {
//    std::string in1("1.0, 2.3, 34.5");
//    double result;
//    parse_numbers_sum(in1.begin(), in1.end(), result);
//    EXPECT_DOUBLE_EQ(37.8, result);
//}
//
// template <typename Iterator>
// bool parse_complex(Iterator first, Iterator end, std::complex<double>& c) {
//    double rN = 0.0;
//    double iN = 0.0;
//
//    auto fr = [&](auto& ctx) { rN = x3::_attr(ctx); };
//    auto fi = [&](auto& ctx) { iN = x3::_attr(ctx); };
//    bool r = x3::phrase_parse(
//            first,
//            end,
//            // Begin grammar
//            (
//                    "(" >> x3::double_[fr] >> -("," >> x3::double_[fi]) >> ")"
//                    | x3::double_[fr]
//                            // End grammar
//            ),
//            x3::ascii::space
//    );
//
//    if(!r || first != end)  //fail if we didn't go to end
//        return false;
//    c = std::complex<double>(rN, iN);
//    return r;
//}
//
// TEST(Spirit_test, complex_alt) {
//    std::string in1("(1.0, 2.0)");
//    std::string in2("(-1.0)");
//    std::string in3("90.0");
//
//    std::complex<double> c1;
//    std::complex<double> c2;
//    std::complex<double> c3;
//
//    parse_complex(in1.begin(), in1.end(), c1);
//    parse_complex(in2.begin(), in2.end(), c2);
//    parse_complex(in3.begin(), in3.end(), c3);
//
//    EXPECT_DOUBLE_EQ(c1.real(), 1.0);
//    EXPECT_DOUBLE_EQ(c1.imag(), 2.0);
//    EXPECT_DOUBLE_EQ(c2.real(), -1.0);
//    EXPECT_DOUBLE_EQ(c3.real(), 90.0);
//}
//
// class print_action {
// public:
//    template <typename Context>
//    void operator()(Context const& ctxt) const {
//        std::cout << x3::_attr(ctxt) << std::endl;
//    }
//};
//
// TEST(Spirit_test, semantic_print_action) {
//    std::string in("{123}");
//    x3::parse(
//            in.begin(),
//            in.end(),
//            "{" >> x3::int_[print_action()] >> "}"
//    );
//}
//
// TEST(Spirit_test, pair_complex) {
//    std::string input("(1.0, 2.0)");
//    std::pair<double, double> p;
//
//    x3::parse(input.begin(), input.end(),
//    "(" >> x3::double_ >> ", " >> x3::double_ >> ")",
//    p);
//
//    EXPECT_DOUBLE_EQ(p.first, 1.0);
//    EXPECT_DOUBLE_EQ(p.second, 2.0);
//}
//
// TEST(Spirit_test, pair_double) {
//    std::string input("1.0 20.0");
//    std::pair<double, double> p;
//    std::string::iterator strbegin = input.begin();
//    x3::phrase_parse(strbegin, input.end(),
//    x3::double_ >> x3::double_, // parser grammar
//    x3::space,                  // delimiter grammar
//    p);
//    EXPECT_DOUBLE_EQ(p.first, 1.0);
//    EXPECT_DOUBLE_EQ(p.second, 20.0);
//}

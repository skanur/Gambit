//
// Created by sudeep on 11/02/17.
//

#include <ParserHelper.h>
#include <ConvertVisitor.h>

#include "Test_Libs.h"

using namespace nlohmann;

class BuildConvertTypesFixture : public ::testing::Test {
public:
  BuildConvertTypesFixture();
  ~BuildConvertTypesFixture() override = default;

protected:
  std::string json_filename;
  std::string input_filename;
  std::vector<std::unique_ptr<ConvertTypes> > all_types;
  ConvertVisitor cv{};
};

BuildConvertTypesFixture::BuildConvertTypesFixture() :json_filename{"../../data/modbus_config.json"}, input_filename{"../../data/sample_input1.dat"} {}

TEST_F(BuildConvertTypesFixture, get_vector) {
  // RVO should kick-in here
  std::vector<ModBusRow> config_rows =
      ParserHelper::parse_ModBusConfig(json_filename);
  for (auto &&row : config_rows) {
    std::cout << row.var_name << ", " << row.unit << std::endl;
  }
}

TEST_F(BuildConvertTypesFixture, builds_successfully) {
  all_types = ParserHelper::build_convert_types(json_filename, input_filename);
  SUCCEED();
}

TEST_F(BuildConvertTypesFixture, check_size) {
  all_types = ParserHelper::build_convert_types(json_filename, input_filename);
  ASSERT_EQ(all_types.size(), 11);
}

TEST_F(BuildConvertTypesFixture, check_real) {
  all_types = ParserHelper::build_convert_types(json_filename, input_filename);
  Real* r = static_cast<Real*>(all_types[0].get());
  r->accept(cv);
  ASSERT_FLOAT_EQ(r->_real, 7.101173400878906);
}

TEST_F(BuildConvertTypesFixture, check_long) {
  all_types = ParserHelper::build_convert_types(json_filename, input_filename);
  Long* l = static_cast<Long*>(all_types[1].get());
  l->accept(cv);
  ASSERT_EQ(l->_long, -56);
}

TEST_F(BuildConvertTypesFixture, check_signal_quality) {
  all_types = ParserHelper::build_convert_types(json_filename, input_filename);
  PartitionedInteger* l = static_cast<PartitionedInteger*>(all_types[2].get());
  l->accept(cv);
  ASSERT_EQ(l->_integer, 38);
}


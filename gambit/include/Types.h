//
// Created by sudeep on 10/02/17.
//

#ifndef GAMBIT_TYPES_H
#define GAMBIT_TYPES_H

#include <string>
#include <cstdint>
#include <vector>

using t_value = uint16_t;
using t_reg = unsigned int;

union conv {
  t_value halfword[2];
  float real_val;
  int32_t long_val;
  uint32_t ulong_val;
};

// Convenient masks
enum MASK {
  HIGH_HW = 0xff00,
  LOW_HW = 0x00ff,
  None
};

enum class format {
  real4,
  long4,
  bcd,
  calendar,
  integer,
  boundedint,
  partitionedint,
  language
};

// TODO: Use const char* instead of string
// Not much operation is going on with string anyways
struct ModBusRow {
  std::string var_name;
  std::string unit;
  format f;
  MASK mask;
  int bound;
  std::vector<int> registers;
};

#endif //GAMBIT_TYPES_H

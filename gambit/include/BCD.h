//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_BCD_H
#define GAMBIT_BCD_H

#include "ConvertTypes.h"
#include "Types.h"

#include <string>
#include <vector>

class BCD : public ConvertTypes {
public:
  std::string _bcd;
  std::vector<std::pair<t_reg, t_value>> _reg_val_pairs;

  BCD(const string &var_name, const string &unit,
      std::vector<std::pair<t_reg, t_value>> &reg_val_pairs);

  void accept(AbstractConvertVisitor &visitor) override;

  ~BCD() override;

protected:
  string toString() override;
};

#endif // GAMBIT_BCD_H

//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_CONVERTVISITOR_H
#define GAMBIT_CONVERTVISITOR_H

#include "AbstractConvertVisitor.h"

class ConvertVisitor : public AbstractConvertVisitor {
public:
  void visit(Real *r) override;

  void visit(Long *l) override;

  void visit(BCD *b) override;

  void visit(CalendarBCD *cb) override;

  void visit(Integer *i) override;

  void visit(BoundedInteger *bi) override;

  void visit(PartitionedInteger *pi) override;

  void visit(LanguageInteger *li) override;

  ~ConvertVisitor() override;
};

#endif // GAMBIT_CONVERTVISITOR_H

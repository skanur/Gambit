//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_REAL_H
#define GAMBIT_REAL_H

#include "ConvertTypes.h"
#include "Types.h"

class Real : public ConvertTypes {
protected:
  string toString() override;

public:
  t_value _lshw_val; // Value stored in least significant half word register
  t_value _mshw_val; // Value stored in most significant half word register
  float _real{};

  Real(const string &var_name, const string &unit, t_value lshw_val,
       t_value mshw_val);

  void accept(AbstractConvertVisitor &visitor) override;

  ~Real() override;
};

#endif // GAMBIT_REAL_H

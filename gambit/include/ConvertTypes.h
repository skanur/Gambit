//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_CONVERTTYPES_H
#define GAMBIT_CONVERTTYPES_H

#include "Types.h"

#include <string>
#include <cstdint>

using std::string;

class AbstractConvertVisitor;

class ConvertTypes {
protected:
  string _var_name;
  string _unit;

  virtual string toString() = 0;

public:
  ConvertTypes(string var_name, string unit);

  const string &get_unit() const;

  void set_unit(const string &unit);

  const string &get_var_name() const;

  void set_var_name(const string &var_name);

  virtual void accept(AbstractConvertVisitor &) = 0;

  virtual string show();

  virtual ~ConvertTypes();
};

#endif // GAMBIT_CONVERTTYPES_H

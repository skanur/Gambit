//
// Created by sudeep on 08/02/17.
//

#ifndef GAMBIT_ABSTRACTPARSEDAST_H
#define GAMBIT_ABSTRACTPARSEDAST_H

#include "Libs.h"
#include "Types.h"
#include <cstdint>

using std::string;

//! Abstract class for holding parsed input data
class AbstractParsedAST {
public:
  /// Get parsed date
  ///
  /// The returned format depends on the overriding fuction
  /// \return String with Year,Month,Date fields
  virtual string get_date() const = 0;

  /// Get parsed time
  ///
  /// The returned format depends on the overriding fuction
  /// \return String with Hour,Mintues fields
  virtual string get_time() const = 0;

  /// Get complete parsed timestamp
  ///
  /// The returned format depends on the overriding fuction
  /// \return String with Year,Month,Date,Hour,Mintues
  virtual string get_timestamp() const = 0;

  /// Get container of register and its contents
  ///
  /// \return Unordered_map with Key=register and Value= register contents
  virtual std::map<t_reg, t_value> get_regVals() = 0;

  /// Push register and its contents to the container of the class
  ///
  /// \param reg_num The register number
  /// \param value  The contents of the register number
  virtual void push_reg_value(t_reg reg_num, t_value value) = 0;

  /// Set the day
  ///
  /// \param day Parse day value
  virtual void set_day(unsigned int day) = 0;

  /// Set the month
  ///
  /// \param month Parse month value
  virtual void set_month(unsigned int month) = 0;

  /// Set the year
  ///
  /// \param year Parse year value
  virtual void set_year(unsigned int year) = 0;

  /// Set the hour
  ///
  /// \param hour Parse hour value
  virtual void set_hour(unsigned int hour) = 0;

  /// Set the mintues
  ///
  /// \param minutes Parse minutes value
  virtual void set_minutes(unsigned int minutes) = 0;

  /// Push the register number
  ///
  /// This always occurs in pair with register. So one can
  /// push register first and then pass its corresponding value
  /// \param reg_num The register number
  virtual void push_reg_num(t_reg reg_num) = 0;

  /// Push value contained in the register
  ///
  /// This always occurs in pair with register. So one can push
  /// register first and then pass its corresponding value
  /// \param value The value associated with the register
  virtual void push_value(t_value value) = 0;

  virtual ~AbstractParsedAST();
};

#endif // GAMBIT_ABSTRACTPARSEDAST_H

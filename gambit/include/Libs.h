//
// Created by sudeep on 09/02/17.
//

#ifndef GAMBIT_LIBS_H
#define GAMBIT_LIBS_H

// Avoid warnings for these libraries
#ifdef __GNUC__
#pragma GCC system_header
#endif

#ifdef __clang__
#pragma clang system_header
#endif

// TODO: Add similar ignores for MSVC

#include "json.hpp"
#include "pegtl.hh"

#endif //GAMBIT_LIBS_H

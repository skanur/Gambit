//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_LANGUAGEINTEGER_H
#define GAMBIT_LANGUAGEINTEGER_H

#include "BoundedInteger.h"

class LanguageInteger : public Integer {
public:
  std::string _lang;

  LanguageInteger(const string &_var_name, const string &_unit,
                  t_value _integer);

  void accept(AbstractConvertVisitor &visitor) override;

  ~LanguageInteger() override;

protected:
  string toString() override;
};

#endif // GAMBIT_LANGUAGEINTEGER_H

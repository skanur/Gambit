//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_BOUNDEDINTEGER_H
#define GAMBIT_BOUNDEDINTEGER_H

#include "Integer.h"
#include "Types.h"

class BoundedInteger : public Integer {
public:
  int _max_bound;

  BoundedInteger(const string &var_name, const string &unit, t_value integer,
                 int max_bound);

  void accept(AbstractConvertVisitor &visitor) override;

  ~BoundedInteger() override;
};

#endif // GAMBIT_BOUNDEDINTEGER_H

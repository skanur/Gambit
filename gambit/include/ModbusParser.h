//
// Created by sudeep on 08/02/17.
//

#ifndef GAMBIT_MODBUSPARSER_H
#define GAMBIT_MODBUSPARSER_H

#include "AbstractParsedAST.h"
#include "Types.h"

// Has pegtl.hh in it

//! Input Register Values Parser definitions
namespace modbusparser {

using namespace pegtl;
//! [Terminal] Year
struct year: must<digit, digit, digit, digit> {};

//! [Terminal] Month
struct month: must<digit, digit> {};

//! [Terminal] Day
struct day: must<digit, digit> {};

//! [Terminal] Hour
struct hour: must<digit, digit> {};

//! [Terminal] Minutes
struct minutes: must<digit, digit> {};

//! [Non-Terminal] Entire date: year-month-day
struct date: must<year, pegtl::one<'-'>, month, pegtl::one<'-'>, day>{};

//! [Non-Terminal] Time: hour:mintues
struct time: must<hour, pegtl::one<':'>, minutes> {};

//! [Non-Terminal] Entire timestamp: date time
struct timestamp: must<date, pegtl::space, time> {};

//! [Terminal] Register number
struct reg_num: plus<digit> {};

//! [Terminal] Value that a register holds
struct value: plus<digit> {};

//! [Non-Terminal] Register value pair: reg_num:value
struct reg_value: must<reg_num, pegtl::one<':'>, value, pegtl::eol> {};

//! [Non-Terminal] All register values: [reg_num:value(newline)]*
struct regs_values: plus<reg_value> {};

//! [Start] grammar
struct grammar: must<timestamp, pegtl::space, regs_values, pegtl::eolf>{};

///////////////////// Actions////////////////////////

// unused attribute is used to avoid compiler warnings
// These structs ARE used while generating templated code, but
// not at runtime. Hence!

//! Default empty action
template <typename Rule>
struct action : pegtl::nothing<Rule> {} ;

//! Action to collect hour
template<>
struct action<hour> {
  template <typename Input>
   static void apply(const Input &in, const std::shared_ptr<AbstractParsedAST>& p) {
    unsigned int hour = static_cast<unsigned int>(std::stoi(in.string()));
    p->set_hour(hour);
  }
} ;

//! Action to collect minutes
template<>
struct action<minutes> {
  template <typename Input>
   static void apply(const Input &in, const std::shared_ptr<AbstractParsedAST>& p) {
    unsigned int minutes = static_cast<unsigned int>(std::stoi(in.string()));
    p->set_minutes(minutes);
  }
} ;

// Action to collect _day
template<>
struct action<day> {
  template <typename Input>
   static void apply(const Input &in, const std::shared_ptr<AbstractParsedAST>& p) {
    unsigned int day = static_cast<unsigned int>(std::stoi(in.string()));
    p->set_day(day);
  }
} ;

// Action to collect _month
template<>
struct action<month> {
  template <typename Input>
   static void apply(const Input &in, const std::shared_ptr<AbstractParsedAST>& p) {
    unsigned int month = static_cast<unsigned int>(std::stoi(in.string()));
    p->set_month(month);
  }
} ;

// Action to collect _year
template<>
struct action<year> {
  template <typename Input>
   static void apply(const Input &in, const std::shared_ptr<AbstractParsedAST>& p) {
    unsigned int year = static_cast<unsigned int>(std::stoi(in.string()));
    p->set_year(year);
  }
} ;

// Action to collect register numbers
template <>
struct action<reg_num> {
  template <typename Input>
  static void apply(const Input &in, const std::shared_ptr<AbstractParsedAST>& p) {
    t_reg reg = static_cast<t_reg>(std::stoi(in.string()));
    p->push_reg_num(reg);
  }
};

// Action to collect values associated with registers
template <>
struct action<value> {
  template <typename Input>
  static void apply(const Input &in, const std::shared_ptr<AbstractParsedAST>& p) {
    t_value value = static_cast<t_value>(std::stoi(in.string()));
    p->push_value(value);
  }
};

} // namespace modbusparser

#endif //GAMBIT_MODBUSPARSER_H

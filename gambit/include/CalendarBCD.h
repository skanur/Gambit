//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_CALENDARBCD_H
#define GAMBIT_CALENDARBCD_H

#include "BCD.h"
#include "Types.h"

class CalendarBCD : public BCD {
public:
  CalendarBCD(const string &_var_name, const string &_unit,
              std::vector<std::pair<t_reg, t_value> > &_reg_val_pairs);

  void accept(AbstractConvertVisitor &visitor) override;

  ~CalendarBCD() override;
};

#endif // GAMBIT_CALENDARBCD_H

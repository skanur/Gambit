//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_VISITOR_H
#define GAMBIT_VISITOR_H

#include "BCD.h"
#include "BoundedInteger.h"
#include "CalendarBCD.h"
#include "Integer.h"
#include "LanguageInteger.h"
#include "Long.h"
#include "PartitionedInteger.h"
#include "Real.h"

class AbstractConvertVisitor {
public:
  virtual void visit(Real *r) = 0;

  virtual void visit(Long *l) = 0;

  virtual void visit(BCD *b) = 0;

  virtual void visit(CalendarBCD *cb) = 0;

  virtual void visit(Integer *i) = 0;

  virtual void visit(BoundedInteger *bi) = 0;

  virtual void visit(PartitionedInteger *pi) = 0;

  virtual void visit(LanguageInteger *li) = 0;

  virtual ~AbstractConvertVisitor();
};

#endif // GAMBIT_VISITOR_H

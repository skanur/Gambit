//
// Created by Sudeep Kanur on 10/02/17.
// Copyright (c) 2017 Sudeep Kanur. All rights reserved.
//
#ifndef GAMBIT_PARSEDVALUEEXCEPTION_H
#define GAMBIT_PARSEDVALUEEXCEPTION_H

#include <stdexcept>

class ParsedValueException : public std::runtime_error {
public:
  ParsedValueException(const std::string &__arg);

  ParsedValueException(const ParsedValueException&);

  ~ParsedValueException() override;
};

#endif //GAMBIT_PARSEDVALUEEXCEPTION_H

//
// Created by Sudeep Kanur on 10/02/17.
// Copyright (c) 2017 Sudeep Kanur. All rights reserved.
//
#ifndef GAMBIT_BUILDCONVERTTYPES_H
#define GAMBIT_BUILDCONVERTTYPES_H

#include "../include/ConvertTypes.h"
#include "../include/AbstractParsedAST.h"
#include "Types.h"

class ParserHelper {
private:
  ParserHelper();

  ~ParserHelper();

public:

  static std::vector<std::unique_ptr<ConvertTypes> > build_convert_types(const std::vector<ModBusRow> &,
                                                                         const std::map<t_reg, t_value> &);

  inline static std::vector<std::unique_ptr<ConvertTypes> > build_convert_types(const std::map<t_reg, t_value> & input,
                                                                         const std::vector<ModBusRow> & config) {
    return build_convert_types(config, input);
  }

  static std::vector<std::unique_ptr<ConvertTypes> > build_convert_types(const std::string& json_filename,
                                                                         const std::string& input_filename);

  static std::map<t_reg, t_value> parse_input(const std::string& input_filename);

  /**
   * Read a json file consisting configuration into a collection of ModBusRow.
   * Check dat folder for examples
   * @param json_filename Either absolute path or relative path (to the
   * executable) of the json file
   * @return const Vector consisting of ModbusRow values. Returning vector
   * should be RVO
   */
  static const std::vector<ModBusRow> parse_ModBusConfig(const std::string& json_filename);
};

#endif //GAMBIT_BUILDCONVERTTYPES_H

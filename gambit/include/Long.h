//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_LONG_H
#define GAMBIT_LONG_H

#include "ConvertTypes.h"
#include "Types.h"

class Long : public ConvertTypes {
public:
  t_value _lshw_val;
  t_value _mshw_val;
  int32_t _long{};

  Long(const string &var_name, const string &unit, t_value lshw_val,
       t_value mshw_val);

  void accept(AbstractConvertVisitor &visitor) override;

  ~Long() override;

protected:
  string toString() override;
};

#endif // GAMBIT_LONG_H

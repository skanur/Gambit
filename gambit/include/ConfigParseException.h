//
// Created by Sudeep Kanur on 11/02/17.
// Copyright (c) 2017 Sudeep Kanur. All rights reserved.
//
#ifndef GAMBIT_CONFIGPARSEEXCEPTION_H
#define GAMBIT_CONFIGPARSEEXCEPTION_H

#include <stdexcept>

class ConfigParseException : public std::runtime_error {
public:
  ConfigParseException(const std::string &__arg);

  ConfigParseException(const ConfigParseException&);

  virtual ~ConfigParseException();
};

#endif //GAMBIT_CONFIGPARSEEXCEPTION_H

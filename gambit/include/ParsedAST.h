//
// Created by sudeep on 08/02/17.
//

#ifndef GAMBIT_PARSEDAST_H
#define GAMBIT_PARSEDAST_H

#include "AbstractParsedAST.h"

//! One concrete implementation of parsed AST
class ParsedAST : public AbstractParsedAST {
private:
  bool _pushed_reg_num{false};
protected:
  unsigned int _day{};
  unsigned int _month{};
  unsigned int _year{};
  unsigned int _hour{};
  unsigned int _minutes{};
  t_reg _reg_num{};
  std::map<t_reg, t_value> _reg_values;

public:
  ParsedAST();

  /// Get complete parsed date
  ///
  /// \return Return date in "Year-Month-Day" format
  string get_date() const override;

  /// Get complete parsed time
  ///
  /// \return Return time "Hour:Minutes" format
  string get_time() const override;

  /// Get complete parsed timestamp
  ///
  /// \return Return in "Year-Month-Day Hour:Minutes" format
  string get_timestamp() const override;

  std::map<t_reg, t_value> get_regVals() override;

  void push_reg_value(t_reg reg_num, t_value value) override;

  void set_day(unsigned int day) override;

  void set_month(unsigned int month) override;

  void set_year(unsigned int year) override;

  void set_hour(unsigned int hour) override;

  void set_minutes(unsigned int minutes) override;

  void push_reg_num(t_reg reg_num) override;

  void push_value(t_value value) override;

  ~ParsedAST() override;
};

#endif //GAMBIT_PARSEDAST_H

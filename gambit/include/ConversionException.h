//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_CONVERSIONEXCEPTION_H
#define GAMBIT_CONVERSIONEXCEPTION_H

#include <stdexcept>
#include <string>

/**
 * Call this exception during conversion of register values to appropriate types
 */
class ConversionException : public std::runtime_error {
public:
  ConversionException(const std::string &__arg);

  ConversionException(const ConversionException&);

  ~ConversionException() override;
};

#endif // GAMBIT_CONVERSIONEXCEPTION_H

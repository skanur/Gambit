//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_INTEGER_H
#define GAMBIT_INTEGER_H

#include "ConvertTypes.h"
#include "Types.h"

class Integer : public ConvertTypes {
public:
  t_value _integer;

  Integer(const string &var_name, const string &unit, t_value integer);

  void accept(AbstractConvertVisitor &visitor) override;

  ~Integer() override;

protected:
  string toString() override;
};

#endif // GAMBIT_INTEGER_H

//
// Created by sudeep on 02/02/17.
//

#ifndef GAMBIT_PARTITIONEDINTEGER_H
#define GAMBIT_PARTITIONEDINTEGER_H

#include "BoundedInteger.h"
#include "Types.h"

class PartitionedInteger : public BoundedInteger {
public:
  MASK _halfword_mask;

  PartitionedInteger(const string &var_name, const string &unit,
                     t_value integer, int max_bound, MASK halfword_mask);

  void accept(AbstractConvertVisitor &visitor) override;

  ~PartitionedInteger() override;
};

#endif // GAMBIT_PARTITIONEDINTEGER_H

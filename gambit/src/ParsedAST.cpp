//
// Created by sudeep on 08/02/17.
//

#include "../include/ParsedValueException.h"

#include <map>
#include "../include/ParsedAST.h"
#include "../include/Types.h"

ParsedAST::ParsedAST() : _pushed_reg_num(false){}

std::string ParsedAST::get_date() const {
  std::ostringstream date_stream("");
  date_stream << std::setfill('0') << std::setw(4) << _year << "-";
  date_stream << std::setfill('0') << std::setw(2) << _month << "-" << std::setw(2) << _day;
  return date_stream.str();
}

std::string ParsedAST::get_time() const {
  std::ostringstream time_stream("");
  time_stream << std::setfill('0') << std::setw(2) << _hour << ":" << std::setw(2) << _minutes;
  return time_stream.str();
}

std::string ParsedAST::get_timestamp() const {
  return get_date() + " " + get_time();
}
std::map<t_reg, t_value> ParsedAST::get_regVals() {
  return _reg_values;
}

void ParsedAST::push_reg_value(t_reg reg_num, t_value value) {
  _reg_values.emplace(reg_num, value);
  _pushed_reg_num = false;
}

void ParsedAST::set_day(unsigned int day) {
  ParsedAST::_day = day;
}

void ParsedAST::set_month(unsigned int month) {
  ParsedAST::_month = month;
}

void ParsedAST::set_year(unsigned int year) {
  ParsedAST::_year = year;
}

void ParsedAST::set_hour(unsigned int hour) {
  ParsedAST::_hour = hour;
}

void ParsedAST::set_minutes(unsigned int minutes) {
  ParsedAST::_minutes = minutes;
}

void ParsedAST::push_reg_num(t_reg reg_num) {
  if(_pushed_reg_num) {
    throw ParsedValueException("Cannot push new register when contents for old register is still not pushed");
  }
  ParsedAST::_reg_num = reg_num;
  _pushed_reg_num = true;
}

void ParsedAST::push_value(t_value value) {
  if(!_pushed_reg_num) {
    throw ParsedValueException("Cannot push a value without pushing its associated register first");
  }
  ParsedAST::push_reg_value(_reg_num, value);
  _pushed_reg_num = false;
}

ParsedAST::~ParsedAST() = default;

//
// Created by sudeep on 02/02/17.
//
#include "../include/BCD.h"
#include "../include/AbstractConvertVisitor.h"

void BCD::accept(AbstractConvertVisitor &visitor) { visitor.visit(this); }

string BCD::toString() { return _bcd; }

BCD::BCD(const string &var_name, const string &unit,
         std::vector<std::pair<t_reg, t_value>> &reg_val_pairs)
    : ConvertTypes(var_name, unit), _reg_val_pairs(reg_val_pairs) {}

BCD::~BCD() = default;

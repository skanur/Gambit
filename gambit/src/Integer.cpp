//
// Created by sudeep on 02/02/17.
//

#include "../include/Integer.h"
#include "../include/AbstractConvertVisitor.h"

void Integer::accept(AbstractConvertVisitor &visitor) { visitor.visit(this); }

string Integer::toString() { return std::to_string(_integer); }

Integer::Integer(const string &var_name, const string &unit, t_value integer)
    : ConvertTypes(var_name, unit), _integer(integer) {}

Integer::~Integer() = default;

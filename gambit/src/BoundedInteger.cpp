//
// Created by sudeep on 02/02/17.
//

#include "../include/BoundedInteger.h"
#include "../include/AbstractConvertVisitor.h"

BoundedInteger::BoundedInteger(const string &var_name, const string &unit,
                               t_value integer, int max_bound)
    : Integer(var_name, unit, integer), _max_bound(max_bound) {}

void BoundedInteger::accept(AbstractConvertVisitor &visitor) {
  visitor.visit(this);
}

BoundedInteger::~BoundedInteger() = default;

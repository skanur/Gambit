//
// Created by Sudeep Kanur on 10/02/17.
// Copyright (c) 2017 Sudeep Kanur. All rights reserved.
//

#include "../include/Real.h"
#include "../include/ParsedValueException.h"
#include "../include/Long.h"
#include "../include/BCD.h"
#include "../include/CalendarBCD.h"
#include "../include/Integer.h"
#include "../include/BoundedInteger.h"
#include "../include/PartitionedInteger.h"
#include "../include/LanguageInteger.h"
#include "../include/ParsedAST.h"
#include "../include/ModbusParser.h"
#include "../include/ConfigParseException.h"

#include <fstream>

#include "../include/ParserHelper.h"

using namespace nlohmann;

ParserHelper::ParserHelper() = default;

ParserHelper::~ParserHelper() = default;

inline void check_register_size(const format &format,
                                const size_t &register_size,
                                const std::string &var_name) {
  switch(format) {
  case format::real4:
  case format::long4: {
    if(register_size != 2) {
      throw ConfigParseException("Register count is not 2 for the type (Real/Long), the entry: " + var_name);
    }
    break;
  }
  case format::integer:
  case format::boundedint:
  case format::partitionedint:
  case format::language: {
    if(register_size != 1) {
      throw ConfigParseException("Register count is not 1 for the type Integer, entry: " + var_name);
    }
    break;
  }
  case format::calendar: {
    if(register_size != 3) {
      throw ConfigParseException("Register count is not 3 for the type Calendar, entry: " + var_name);
    }
    break;
  }
  case format::bcd:break;
  }
}

inline format get_format(const std::string& format_string) {
  if(format_string == "real4") return format::real4;
  if(format_string == "long4") return format::long4;
  if(format_string == "bcd") return format::bcd;
  if(format_string == "calendar") return format::calendar;
  if(format_string == "integer") return format::integer;
  if(format_string == "boundedint") return format::boundedint;
  if(format_string == "partitionedint") return format::partitionedint;
  if(format_string == "language") return format::language;

  // No such type available, if reached here
  throw ConfigParseException("Config file has no such format: " + format_string);
}

inline MASK get_mask(const std::string &mask_string) {
  if(mask_string == "high") return MASK::HIGH_HW;
  if(mask_string == "low") return MASK::LOW_HW;

  // No such type available, if reached here
  throw ConfigParseException("Config file has invalid mask string: " + mask_string);
}

inline const std::pair<t_value, t_value> get_real_value(const ModBusRow& m, const std::map<t_reg, t_value>& input) {
  t_value lshw_val;
  t_value mshw_val;
  t_reg lshw_reg;
  t_reg mshw_reg;
  try {
    lshw_reg = static_cast<t_reg>(m.registers[0]);
    mshw_reg = static_cast<t_reg>(m.registers[1]);
    lshw_val = input.at(lshw_reg);
    mshw_val = input.at(mshw_reg);
  } catch(const std::out_of_range& ) {
    std::ostringstream err_str("");
    err_str << "Register " << lshw_reg << ", " << mshw_reg << " not found for the entry" << m.var_name << std::endl;
    throw ParsedValueException(err_str.str());
  }

  return std::make_pair(mshw_val, lshw_val);
}

inline t_value get_integer_value(const ModBusRow& m, const std::map<t_reg, t_value> &input) {
  t_reg reg_num = 0;
  t_value val = 0;
  try {
    reg_num = static_cast<t_reg>(m.registers[0]);
    val = input.at(reg_num);
  } catch (const std::out_of_range&) {
    std::ostringstream err_str("");
    err_str << "Register " << reg_num << " not found for the entry: " << m.var_name << std::endl;
    throw ParsedValueException(err_str.str());
  }

  return val;
}

std::vector<std::unique_ptr<ConvertTypes> > ParserHelper::build_convert_types(const std::vector<ModBusRow> & config, const std::map<t_reg, t_value>& input) {

  std::vector<std::unique_ptr<ConvertTypes> > types;

  for (auto &c : config) {
    switch(c.f) {
    case format::real4:{
      t_value lshw_val, mshw_val;
      std::tie(mshw_val, lshw_val) = get_real_value(c, input);
      types.emplace_back(std::unique_ptr<Real>(new Real(c.var_name, c.unit, lshw_val, mshw_val)));

      break;
    }

    case format::long4: {
      t_value lshw_val, mshw_val;
      std::tie(mshw_val, lshw_val) = get_real_value(c, input);
      types.emplace_back(std::unique_ptr<Long>(new Long(c.var_name, c.unit, lshw_val, mshw_val)));

      break;
    }

    case format::bcd: {
      std::vector<std::pair<t_reg, t_value> > reg_value_pairs;
      t_reg reg_num;
      try {
        for (const auto &r : c.registers) {
          reg_num = static_cast<t_reg>(r);
          t_value val = input.at(static_cast<unsigned int>(r));
          reg_value_pairs.emplace_back(r, val);
        }
      } catch(const std::out_of_range&) {
        std::ostringstream err_str("");
        err_str << "Register " << reg_num << " not found for the entry: " << c.var_name << std::endl;
        throw ParsedValueException(err_str.str());
      }
      types.emplace_back(std::unique_ptr<BCD>(new BCD(c.var_name, c.unit, reg_value_pairs)));

      break;
    }

    case format::calendar: {
      std::vector<std::pair<t_reg, t_value> > reg_value_pairs;
      t_reg reg_num;
      try {
        for(const auto &r: c.registers) {
          reg_num = static_cast<t_reg>(r);
          t_value val = input.at(static_cast<unsigned int>(r));
          reg_value_pairs.emplace_back(r, val);
        }
      } catch (const std::out_of_range& ) {
        std::ostringstream err_str("");
        err_str << "Register " << reg_num << " not found for the entry: " << c.var_name << std::endl;
        throw ParsedValueException(err_str.str());
      }
      types.emplace_back(std::unique_ptr<CalendarBCD>(new CalendarBCD(c.var_name, c.unit, reg_value_pairs)));

      break;
    }

    case format::integer: {
      // This function throws an exception, but we won't handle it
      // as we need it to kill the process
      t_value val = get_integer_value(c, input);

      types.emplace_back(std::unique_ptr<Integer>(new Integer(c.var_name, c.unit, val)));

      break;
    }

    case format::boundedint: {
      // This function throws exception, but we won't handle it
      // as we need it to kill the process
      t_value val = get_integer_value(c, input);

      types.emplace_back(std::unique_ptr<BoundedInteger>(new BoundedInteger(c.var_name, std::string(""), val, c.bound)));

      break;
    }

    case format::partitionedint: {
      // This function throws exception, but we won't handle it
      // as we need it to kill the process
      t_value val = get_integer_value(c, input);

      types.emplace_back(std::unique_ptr<PartitionedInteger>(new PartitionedInteger(c.var_name, c.unit, val, c.bound, c.mask)));

      break;
    }

    case format::language: {
      // This function throws an exception, but we won't handle it
      // as we need it to kill the process
      t_value val = get_integer_value(c, input);

      types.emplace_back(std::unique_ptr<LanguageInteger>(new LanguageInteger(c.var_name, c.unit, val)));

      break;
    }

    }
  }

  return types;
}
std::map<t_reg, t_value> ParserHelper::parse_input(const std::string &input_filename) {
  std::shared_ptr<AbstractParsedAST> p{new ParsedAST()};
  try{
    modbusparser::parse_file<modbusparser::grammar, modbusparser::action>(input_filename, p);
  } catch(const std::exception& e){
    std::cerr << e.what() << std::endl;
    exit(1);
  }
  return p->get_regVals();
}

const std::vector<ModBusRow> ParserHelper::parse_ModBusConfig(const std::string &json_filename) {
  std::ifstream json_file_stream;
  json config_json_obj;
  std::vector<ModBusRow> config;

  json_file_stream.open(json_filename);
  // TODO: Someday introduce a logger
  if (json_file_stream.bad() || json_file_stream.fail()) {
    std::cerr << "Could not open: " << json_filename << std::endl;
  } else if (json_file_stream.good()) {
    // Read in the json config file to the object
    try {
      json_file_stream >> config_json_obj;
    } catch (const std::exception &e) {
      json_file_stream.close();
      std::cerr << e.what() << std::endl;
    }
    json_file_stream.close();

    // Allocate vector and read in ModBusRow one by one
    size_t total_rows = config_json_obj["modbus_config"].size();
    struct ModBusRow m;
    for (uint i = 0; i < total_rows; i++) {
      json modbusrow_json = config_json_obj["modbus_config"][i];
      m.var_name = modbusrow_json["variable_name"].get<std::string>();

      // Convert string to enum type
      // Throws an exception, but we won't handle it as it needs
      // to kill the process
      m.f = get_format(modbusrow_json["format"].get<std::string>());

      m.registers = modbusrow_json["registers"].get<std::vector<int> >();
      // Throws an exception, but we won't handle it as it needs to kill
      // the process
      check_register_size(m.f, m.registers.size(), m.var_name);

      if(modbusrow_json.find("mask") != modbusrow_json.end()) {
        // Throws an exception, but we won't handle it as it needs
        // to kill the process
        m.mask = get_mask(modbusrow_json["mask"].get<std::string>());
      } else if(m.f == format::partitionedint) {
        throw ConfigParseException("PartitionedInt type can't have empty mask for entry: " + m.var_name);
      } else {
        m.mask = MASK::None;
      }

      if(modbusrow_json.find("bound") !=  modbusrow_json.end()) {
        m.bound = modbusrow_json["bound"].get<int>();
      } else if(m.f == format::boundedint) {
        throw ConfigParseException("BoundedInt type can't have empty bound for the entry: " + m.var_name);
      } else if(m.f == format::partitionedint) {
        throw ConfigParseException("PartitionedInt type can't have empty bound for the entry: " + m.var_name);
      } else {
        m.bound = -1;
      }

      if(modbusrow_json.find("unit") != modbusrow_json.end()){
        m.unit = modbusrow_json["unit"].get<std::string>();
      } else {
        m.unit = "";
      }

      config.emplace_back(m);
    }
  }
  return config;
}
std::vector<std::unique_ptr<ConvertTypes> > ParserHelper::build_convert_types(const std::string &json_filename,
                                                                              const std::string &input_filename) {
  return build_convert_types(parse_input(input_filename),
                             parse_ModBusConfig(json_filename));
}


//
// Created by sudeep on 02/02/17.
//

#include "../include/Long.h"
#include "../include/AbstractConvertVisitor.h"

void Long::accept(AbstractConvertVisitor &visitor) { visitor.visit(this); }

string Long::toString() { return std::to_string(_long); }

Long::Long(const string &var_name, const string &unit,
           const t_value lshw_val, const t_value mshw_val)
    : ConvertTypes(var_name, unit), _lshw_val(lshw_val),
      _mshw_val(mshw_val) {}
Long::~Long() = default;

//
// Created by sudeep on 02/02/17.
//

#include "../include/CalendarBCD.h"
#include "../include/AbstractConvertVisitor.h"

void CalendarBCD::accept(AbstractConvertVisitor &visitor) {
  visitor.visit(this);
}

CalendarBCD::CalendarBCD(const string &_var_name, const string &_unit,
                         std::vector<std::pair<t_reg, t_value>> &_reg_val_pairs)
    : BCD(_var_name, _unit, _reg_val_pairs) {}

CalendarBCD::~CalendarBCD() = default;

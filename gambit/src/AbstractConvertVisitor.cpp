//
// Created by sudeep on 02/02/17.
//

#include "../include/AbstractConvertVisitor.h"

void AbstractConvertVisitor::visit(Real* /*r*/) {}

void AbstractConvertVisitor::visit(Long* /*l*/) {}

void AbstractConvertVisitor::visit(BCD* /*b*/) {}

void AbstractConvertVisitor::visit(CalendarBCD* /*cb*/) {}

void AbstractConvertVisitor::visit(Integer* /*i*/) {}

void AbstractConvertVisitor::visit(BoundedInteger* /*bi*/) {}

void AbstractConvertVisitor::visit(PartitionedInteger* /*pi*/) {}

void AbstractConvertVisitor::visit(LanguageInteger* /*li*/) {}

AbstractConvertVisitor::~AbstractConvertVisitor() = default;

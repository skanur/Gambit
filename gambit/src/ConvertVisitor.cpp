//
// Created by sudeep on 02/02/17.
//

#include "../include/ConvertVisitor.h"
#include "../include/ConversionException.h"
#include <algorithm>
#include <iomanip>
#include <ios>
#include <sstream>

void ConvertVisitor::visit(Real *r) {
  conv c{
      {
          r->_lshw_val,
          r->_mshw_val
      }
  };
  r->_real = c.real_val;
}

void ConvertVisitor::visit(Long *l) {
  conv c{
      {
          l->_lshw_val,
          l->_mshw_val
      }
  };
  l->_long = c.long_val;
}

void ConvertVisitor::visit(BCD *b) {
  std::ostringstream bcd_stream;
  for (std::vector<std::pair<t_reg, t_value>>::const_reverse_iterator pair_it =
           b->_reg_val_pairs.rbegin();
       pair_it != b->_reg_val_pairs.rend(); pair_it++) {
    t_value reg_val = pair_it->second;
    bcd_stream << std::setw(4) << std::setfill('0') << std::hex << reg_val;
  }

  b->_bcd = bcd_stream.str();
  std::transform(b->_bcd.begin(), b->_bcd.end(), b->_bcd.begin(), ::toupper);
}

void ConvertVisitor::visit(CalendarBCD *cb) {
  visit(static_cast<BCD *>(cb));
  if (cb->_bcd.size() != 12) {
    throw ConversionException("Calendar string size is not 6 bytes");
  }
  int month = 0;
  int day = 0;
  int year = 0;

  for (size_t bcd_char_i = 0; bcd_char_i < 12; bcd_char_i += 2) {
    t_value bcd_chars = static_cast<t_value>(std::stoi(
        cb->_bcd.substr(bcd_char_i, 2))); // Take two bcd characters at a time

    switch (bcd_char_i) {
    case 10:
    case 8: {
      if (bcd_chars > 60 || bcd_chars < 0) {
        std::ostringstream err_stream;
        err_stream << "Calendar's Second/Minute: " << bcd_chars
                   << " is not within 60";
        throw ConversionException(err_stream.str());
      }
      break;
    }
    case 6: {
      if (bcd_chars > 23 || bcd_chars < 0) {
        std::ostringstream err_stream;
        err_stream << "Calendar's Hour: " << bcd_chars << " is not within 24";
        throw ConversionException(err_stream.str());
      }
      break;
    }
    case 4: {
      if (bcd_chars > 31 || bcd_chars < 1) {
        std::ostringstream err_stream;
        err_stream << "Calendar's Date: " << bcd_chars << " is not within 31";
        throw ConversionException(err_stream.str());
      } else {
        day = bcd_chars;
      }
      break;
    }
    case 2: {
      if (bcd_chars > 12 || bcd_chars < 1) {
        std::ostringstream err_stream;
        err_stream << "Calendar's Month: " << bcd_chars << " is not within 12";
        throw ConversionException(err_stream.str());
      } else {
        month = bcd_chars;
      }
      break;
    }
    case 0: {
      if (bcd_chars < 0) {
        std::ostringstream err_stream;
        err_stream << "Calendar's Year: " << bcd_chars << " is not positive";
        throw ConversionException(err_stream.str());
      } else {
        year = bcd_chars;
      }
      break;
    }
    default:break;
    }
  }

  if ((month == 4 || month == 6 || month == 9 || month == 11) && (day > 30)) {
    std::ostringstream err_stream;
    err_stream << "Month: " << month << " can't have more than 30 days";
    throw ConversionException(err_stream.str());
  } else if ((month == 2) && (year % 4 == 0) && (day > 29)) {
    throw ConversionException("Leap _year February can't have 29 days");
  } else if ((month == 2) && (year % 4 != 0) && (day > 28)) {
    throw ConversionException("Non-Leap _year February can't have 28 days");
  }
}

void ConvertVisitor::visit(Integer * /*i*/) {}

void ConvertVisitor::visit(BoundedInteger *bi) {
  if (bi->_integer > bi->_max_bound) {
    std::ostringstream err_stream;
    err_stream << "Integer: " << bi->_integer
               << " can't be above max bound: " << bi->_max_bound;
    throw ConversionException(err_stream.str());
  }
}

void ConvertVisitor::visit(PartitionedInteger *pi) {
  pi->_integer = pi->_integer & static_cast<const int16_t>(pi->_halfword_mask);
  switch (pi->_halfword_mask) {
  case (HIGH_HW): {
    pi->_integer = pi->_integer >> 8;
    break;
  }
  case (LOW_HW): {
    // Do nothing
    break;
  }
  case None:break;
  }
  visit(static_cast<BoundedInteger *>(pi));
}

void ConvertVisitor::visit(LanguageInteger *li) {
  switch (li->_integer) {
  case 0: {
    li->_lang = string("English");
    break;
  }

  case 1: {
    li->_lang = string("Chinese");
    break;
  }

  default: {
    std::ostringstream err_stream;
    err_stream << "Option: " << li->_integer << " not supported";
    throw ConversionException(err_stream.str());
  }
  }
}
ConvertVisitor::~ConvertVisitor() = default;

//
// Created by Sudeep Kanur on 11/02/17.
// Copyright (c) 2017 Sudeep Kanur. All rights reserved.
//
#include "../include/ConfigParseException.h"

ConfigParseException::ConfigParseException(const std::string &__arg) : runtime_error("ConfigParseException: " + __arg) {}

ConfigParseException::ConfigParseException(const ConfigParseException &) = default;

ConfigParseException::~ConfigParseException() = default;

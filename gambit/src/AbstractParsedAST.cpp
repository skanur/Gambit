//
// Created by sudeep on 09/02/17.
//

#include "../include/AbstractParsedAST.h"
#include "../include/Types.h"

string AbstractParsedAST::get_date() const {return std::string();}

string AbstractParsedAST::get_time() const {return std::string();}

string AbstractParsedAST::get_timestamp() const {return std::string();}

std::map<t_reg, t_value> AbstractParsedAST::get_regVals() {return std::map<t_reg, t_value>();}

void AbstractParsedAST::push_reg_value(t_reg /*reg_num*/, t_value /*value*/) {}

void AbstractParsedAST::set_day(unsigned int /*_day*/) {}

void AbstractParsedAST::set_month(unsigned int /*_month*/) {}

void AbstractParsedAST::set_year(unsigned int /*_year*/) {}

void AbstractParsedAST::set_hour(unsigned int /*_hour*/) {}

void AbstractParsedAST::set_minutes(unsigned int /*_minutes*/) {}

AbstractParsedAST::~AbstractParsedAST() = default;


//
// Created by sudeep on 02/02/17.
//

#include "../include/ConversionException.h"

ConversionException::ConversionException(const std::string &__arg)
    : runtime_error("ConversionException: " + __arg) {}

ConversionException::ConversionException(const ConversionException &) = default;

ConversionException::~ConversionException() = default;

//
// Created by sudeep on 02/02/17.
//

#include "../include/LanguageInteger.h"
#include "../include/AbstractConvertVisitor.h"

void LanguageInteger::accept(AbstractConvertVisitor &visitor) {
  visitor.visit(this);
}

string LanguageInteger::toString() { return _lang; }

LanguageInteger::LanguageInteger(const string &_var_name, const string &_unit,
                                 t_value _integer)
    : Integer(_var_name, _unit, _integer) {}

LanguageInteger::~LanguageInteger() = default;

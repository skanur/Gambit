//
// Created by Sudeep Kanur on 10/02/17.
// Copyright (c) 2017 Sudeep Kanur. All rights reserved.
//
#include "../include/ParsedValueException.h"

ParsedValueException::ParsedValueException(const std::string &__arg) : runtime_error("ParsedValueException: " + __arg) {}

ParsedValueException::ParsedValueException(const ParsedValueException &) = default;

ParsedValueException::~ParsedValueException() = default;

//
// Created by sudeep on 02/02/17.
//

#include "../include/PartitionedInteger.h"
#include "../include/AbstractConvertVisitor.h"

void PartitionedInteger::accept(AbstractConvertVisitor &visitor) {
  visitor.visit(this);
}

PartitionedInteger::PartitionedInteger(const string &var_name,
                                       const string &unit, t_value integer,
                                       int max_bound, MASK halfword_mask)
    : BoundedInteger(var_name, unit, integer, max_bound),
      _halfword_mask(halfword_mask) {}

PartitionedInteger::~PartitionedInteger() = default;

//
// Created by sudeep on 02/02/17.
//

#include <sstream>
#include <utility>

#include "../include/ConvertTypes.h"

const string &ConvertTypes::get_var_name() const { return _var_name; }

void ConvertTypes::set_var_name(const string &var_name) {
  ConvertTypes::_var_name = var_name;
}

const string &ConvertTypes::get_unit() const { return _unit; }

void ConvertTypes::set_unit(const string &unit) {
  ConvertTypes::_unit = unit;
}

ConvertTypes::ConvertTypes(string var_name, string unit)
    : _var_name(std::move(var_name)), _unit(std::move(unit)) {}

string ConvertTypes::show() {
  std::ostringstream show_stream;

  show_stream << "Variable: " << _var_name << ", Value: " << toString();
  if (_unit != std::string("")) {
    show_stream << ", Unit: " << _unit;
  }
  show_stream << std::endl;
  return show_stream.str();
}

ConvertTypes::~ConvertTypes() = default;

//
// Created by sudeep on 02/02/17.
//

#include "../include/Real.h"
#include "../include/AbstractConvertVisitor.h"

Real::Real(const string &var_name, const string &unit,
           const t_value lshw_val, const t_value mshw_val)
    : ConvertTypes(var_name, unit), _lshw_val(lshw_val),
      _mshw_val(mshw_val) {}

void Real::accept(AbstractConvertVisitor &visitor) { visitor.visit(this); }

string Real::toString() { return std::to_string(_real); }

Real::~Real() = default;

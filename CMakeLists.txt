cmake_minimum_required(VERSION 3.6)
project(Gambit)

######## Compiler settings
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

### Clang specific
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    # Set everything, and remove some irritating flags
    set(EVERYTHING_FLAG "-Weverything -Wno-c++98-compat -Wno-global-constructors -Wno-padded -Wno-exit-time-destructors")
else()
    set(EVERYTHING_FLAG "")
endif()

######## Include sub projects
add_subdirectory(gambit)
add_subdirectory(gambit_tests)
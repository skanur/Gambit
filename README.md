# Gambit

The project is a C++ solution for the [gambit-labs challenge](https://github.com/gambit-labs/tuf-2000m). 
The application parses a file from a Modbus interface of an ultra sonic sensor and presents a human readable 
output. The output has status of various variables that the sensor keeps track of.
  
## Installation
 
 1. Clone the repo
 2. Initialize the submodules by `git submodules update --init`
 3. Compile and run by
    ```$bash
    mkdir build
    cd build
    cmake ..
    make
    ```
    
## Configuration

In addition to a modbus file, the executable also needs to read a configuration file written in JSON
that specifies the various sensor variables, its unit and type. Check `modbus_config.json` file in the folder
`data` for details.
